# frozen_string_literal: true

class Redis
  class Throttle
    # Gem version.
    VERSION = "0.0.1"
  end
end

# v0.0.1 (2020-08-17)

* Initial release. Effectively this version represents extraction of concurrency
  and threshold strategies from [sidekiq-throttled][].


[sidekiq-throttled]: https://github.com/sensortower/sidekiq-throttled
